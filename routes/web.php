<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


/*Route::group(['namespace'=>'backend'],function (){
   Route::get('/@dashboard@','adminController@login')->name('dashboard-login');
   Route::post('/login-action','adminController@login_action')->name('login-action');
   Route::post('/logout','adminController@logout')->name('dashboard-logout');
});

Route::group(['namespace'=>'backend','prefix'=>'dashboard','middleware'=>'auth'],function (){
    Route::get('/','indexController@index')->name('dashboard');
    Route::get('/profile','adminController@profile')->name('profile');
    Route::get('/edit-profile','adminController@edit_profile')->name('edit-profile');
    Route::post('/edit-profile','adminController@profile_action')->name('profile-action');
    Route::post('/update-password','adminController@update_password')->name('update-password');
    Route::get('/register','adminController@register')->name('register');
    Route::post('/register','adminController@register_action')->name('register-action');
    Route::get('/profile-del','adminController@profile_del')->name('profile-del');
});*/
