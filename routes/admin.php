<?php


Route::group(['namespace'=>'admin'],function (){
Route::get('/','adminController@login')->name('dashboard-login');
Route::post('/login-action','adminController@login_action')->name('login-action');
Route::post('/logout','adminController@logout')->name('dashboard-logout');
});

Route::group(['namespace'=>'admin','prefix'=>'dashboard','middleware'=>'auth'],function (){
Route::get('/','adminController@index')->name('dashboard');
Route::get('/profile','adminController@profile')->name('profile');
Route::get('/edit-profile','adminController@edit_profile')->name('edit-profile');
Route::post('/edit-profile','adminController@profile_action')->name('profile-action');
Route::post('/update-password','adminController@update_password')->name('update-password');
Route::get('/register','adminController@register')->name('register');
Route::post('/register','adminController@register_action')->name('register-action');
Route::get('/profile-del','adminController@profile_del')->name('profile-del');
});