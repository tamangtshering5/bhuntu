@extends('backend.pages.master')
@section('body')
    <!-- page content -->
    <div class="right_col" role="main">
        @if(count($errors)>0)
            @foreach($errors->all() as $error )
                <p class=" alert-success">{{$error}}</p>

            @endforeach
        @endif

        @if(session('success'))
            <p class="alert alert-success">{{session('success')}}</p>
    @endif
    <!-- top tiles -->
        <div class="row tile_count">
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
                <span class="count_top"><i class="fa fa-user"></i> Total Clients</span>
                <div class="count">12</div>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
                <span class="count_top"><i class="fa fa-user"></i> Active Clients</span>
                <div class="count green">22</div>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
                <span class="count_top"><i class="fa fa-user"></i> Inactive Clients</span>
                <div class="count red">21</div>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
                <span class="count_top"><i class="fa fa-clock-o"></i> Expery in This week </span>
                <div class="count">21</div>

            </div>
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
                <span class="count_top"><i class="fa fa-clock-o"></i> Expery in This Month </span>
                <div class="count">12</div>

            </div>
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
                <span class="count_top"><i class="fa fa-clock-o"></i>  Expired   </span>
                <div class="count">212</div>

            </div>
        </div>
        <!-- /top tiles -->

        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="dashboard_graph">

                    <h1>welcome</h1>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
    <!-- /page content -->
@endsection