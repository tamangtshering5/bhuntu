<div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
    <div class="menu_section">
        <h3>Menu</h3>
        <ul class="nav side-menu">

            <li><a href="{{--{{route('home')}}--}}"> <i class="fa fa-home"></i>  Home <span class="fa fa-chevron-right"></span></a>
            </li>
            <li><a href="{{--{{route('clients')}}--}}"><i class="fa fa-group"></i>  Clients <span class="fa fa-chevron-right"></span></a>
            </li>
            <li><a href="{{--{{route('employee')}}--}}"><i class="fa fa-group"></i> Employees  <span class="fa fa-chevron-right"></span></a>
            </li>
            <li><a href="{{--{{route('leave')}}--}}"><i class="fa fa-life-ring" ></i> leave and Holiday <span class="fa fa-chevron-right"></span></a>

            </li>
            <li><a href="#"><i class="fa fa-briefcase"></i> Projects  <span class="fa fa-chevron-down"></span></a>
                <ul class="nav child_menu">
                    <li><a href="{{--{{route('upcoming')}}--}}">Upcoming Projects</a></li>
                    <li><a href="{{--{{route('running')}}--}}">Running Projects</a></li>
                    <li><a href="{{--{{route('completed')}}--}}">Completed Projects</a></li>
                </ul>

            </li>

        </ul>
    </div>
</div>
<!-- /sidebar menu -->

<!-- /menu footer buttons -->
<div class="sidebar-footer hidden-small">
    <h6 style="text-align: center;"><strong>&copy; <br/>Copyright Bhutnu the collection<br/> All Rights Reserved.</strong></h6>
</div>
<!-- /menu footer buttons -->
</div>
</div>