{{--//////////////////////////////////////////////////////////////////////////////--}}

                <body class="nav-md">
                <div class="container body">
                    <div class="main_container">
                        <div class="col-md-3 left_col">
                            <div class="left_col scroll-view">
                                <div class="navbar nav_title" style="border: 0;">
                                    <a href="{{--{{route('home')}}--}}" class="site_title"><i class="fa fa-paw"></i> <span>Bhuntu!</span></a>
                                </div>

                                <div class="clearfix"></div>

                                <!-- menu prile quick info -->
                                <div class="profile">
                                    <div class="profile_pic">
                                        @if(empty(Auth::user()->image))
                                            <img src="{{URL::to('backend/images/profile/dummy.png')}}" alt="Avatar" class="img-circle profile_img">
                                        @else
                                            <img src="{{URL::to('backend/images/profile/'.Auth::user()->image)}}" alt="..." class="img-circle profile_img">                                        @endif
                                    </div>
                                    <div class="profile_info">
                                        <span>Welcome,</span>
                                        <h2>{{Auth::user()->name}}</h2>
                                    </div>
                                </div>
                                <!-- /menu prile quick info -->

                                <br />






