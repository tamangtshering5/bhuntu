
{{--/////////////////////////////////////--}}
<!-- footer content -->
<footer>
    <div class="pull-right">
        License to  <a href="#">Bhuntu the collection</a>
    </div>
    <div class="clearfix"></div>
</footer>
<!-- /footer content -->
</div>
</div>
<div id="custom_notifications" class="custom-notifications dsp_none">
    <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
    </ul>
    <div class="clearfix"></div>
    <div id="notif-group" class="tabbed_notifications"></div>
</div>
<!-- jQuery -->
<script src="{{URL::to('backend/vendors/jquery/dist/jquery.min.js')}}"></script>
<script style="display: none !important;">!function(e,t,r,n,c,a,l){function i(t,r){return r=e.createElement('div'),r.innerHTML='<a href="'+t.replace(/"/g,'&quot;')+'"></a>',r.childNodes[0].getAttribute('href')}function o(e,t,r,n){for(r='',n='0x'+e.substr(t,2)|0,t+=2;t<e.length;t+=2)r+=String.fromCharCode('0x'+e.substr(t,2)^n);return i(r)}try{for(c=e.getElementsByTagName('a'),l='/cdn-cgi/l/email-protection#',n=0;n<c.length;n++)try{(t=(a=c[n]).href.indexOf(l))>-1&&(a.href='mailto:'+o(a.href,t+l.length))}catch(e){}for(c=e.querySelectorAll('.__cf_email__'),n=0;n<c.length;n++)try{(a=c[n]).parentNode.replaceChild(e.createTextNode(o(a.getAttribute('data-cfemail'),0)),a)}catch(e){}}catch(e){}}(document);</script><script src="{{URL::to('backend/vendors/jquery/dist/jquery.min.js')}}"></script>

<!-- Bootstrap -->
<script src="{{URL::to('backend/vendors/bootstrap/dist/js/bootstrap.min.js')}}"></script>
<!-- FastClick -->
<script src="{{URL::to('backend/vendors/fastclick/lib/fastclick.js')}}"></script>
<!-- NProgress -->
<script src="{{URL::to('backend/vendors/nprogress/nprogress.js')}}"></script>
<!-- Chart.js -->
<script src="{{URL::to('backend/vendors/Chart.js/dist/Chart.min.js')}}"></script>
<!-- gauge.js -->
<script src="{{URL::to('backend/vendors/gauge.js/dist/gauge.min.js')}}"></script>
<!-- bootstrap-progressbar -->
<script src="{{URL::to('backend/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js')}}"></script>
<!-- iCheck -->
<script src="{{URL::to('backend/vendors/iCheck/icheck.min.js')}}"></script>
<!-- Skycons -->
<script src="{{URL::to('backend/vendors/skycons/skycons.js')}}"></script>
<!-- Flot -->
<script src="{{URL::to('backend/vendors/Flot/jquery.flot.js')}}"></script>
<script src="{{URL::to('backend/vendors/Flot/jquery.flot.pie.js')}}"></script>
<script src="{{URL::to('backend/vendors/Flot/jquery.flot.time.js')}}"></script>
<script src="{{URL::to('backend/vendors/Flot/jquery.flot.stack.js')}}"></script>
<script src="{{URL::to('backend/vendors/Flot/jquery.flot.resize.js')}}"></script>
<!-- Flot plugins -->
<script src="{{URL::to('backend/vendors/flot.orderbars/js/jquery.flot.orderBars.js')}}"></script>
<script src="{{URL::to('backend/vendors/flot-spline/js/jquery.flot.spline.min.js')}}"></script>
<script src="{{URL::to('backend/vendors/flot.curvedlines/curvedLines.js')}}"></script>
<!-- DateJS -->
<script src="{{URL::to('backend/vendors/DateJS/build/date.js')}}"></script>
<!-- JQVMap -->
<script src="{{URL::to('backend/vendors/jqvmap/dist/jquery.vmap.js')}}"></script>
<script src="{{URL::to('backend/vendors/jqvmap/dist/maps/jquery.vmap.world.js')}}"></script>
<script src="{{URL::to('backend/vendors/jqvmap/examples/js/jquery.vmap.sampledata.js')}}"></script>
<!-- bootstrap-daterangepicker -->
<script src="{{URL::to('backend/vendors/moment/min/moment.min.js')}}"></script>
<script src="{{URL::to('backend/vendors/bootstrap-daterangepicker/daterangepicker.js')}}"></script>

<!-- Custom Theme Scripts -->
<script src="{{URL::to('backend/build/js/custom.min.js')}}"></script>
<!-- Google Analytics -->
<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-23581568-13', 'auto');
    ga('send', 'pageview');

</script>

<!-- Datatables -->
<script src="{{URL::to('backend/vendors/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{URL::to('backend/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
<script src="{{URL::to('backend/vendors/datatables.net-buttons/js/dataTables.buttons.min.js')}}"></script>
<script src="{{URL::to('backend/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js')}}"></script>
<script src="{{URL::to('backend/vendors/datatables.net-buttons/js/buttons.flash.min.js')}}"></script>
<script src="{{URL::to('backend/vendors/datatables.net-buttons/js/buttons.html5.min.js')}}"></script>
<script src="{{URL::to('backend/vendors/datatables.net-buttons/js/buttons.print.min.js')}}"></script>
<script src="{{URL::to('backend/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js')}}"></script>
<script src="{{URL::to('backend/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js')}}"></script>
<script src="{{URL::to('backend/vendors/datatables.net-responsive/js/dataTables.responsive.min.js')}}"></script>
<script src="{{URL::to('backend/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js')}}"></script>
<script src="{{URL::to('backend/vendors/datatables.net-scroller/js/dataTables.scroller.min.js')}}"></script>
<script src="{{URL::to('backend/vendors/jszip/dist/jszip.min.js')}}"></script>
<script src="{{URL::to('backend/vendors/pdfmake/build/pdfmake.min.js')}}"></script>
<script src="{{URL::to('backend/vendors/pdfmake/build/vfs_fonts.js')}}"></script>

{{--////////////////script for add more field///////////////--}}
<script type="text/javascript"  >
    $(document).ready(function(){
        var i=1;

        $('#domainadd').click(function(){
            i++;
            $('#dynamic_field').append('<div id="row'+i+'"><br/><br/><br/><br/><div class="form-group"> <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Domain name</label><div class="col-md-6 col-sm-6 col-xs-12"><input id="next_domain" class="form-control col-md-7 col-xs-12" type="text" name="next_domain[]"> </div> <button type="button" name="remove" id="'+i+'" class="btn btn-danger btn_remove">X</button></div><div class="form-group"> <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">FTP username</label> <div class="col-md-6 col-sm-6 col-xs-12"><input id="next_ftpname" class="form-control col-md-7 col-xs-12" type="text" name="next_ftpname[]"> </div> <button type="button" name="remove" id="'+i+'" class="btn btn-danger btn_remove">X</button></div><div class="form-group"> <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">FTP password</label> <div class="col-md-6 col-sm-6 col-xs-12"><input id="next_ftppassword" class="form-control col-md-7 col-xs-12" type="text" name="next_ftppassword[]"> </div> <button type="button" name="remove" id="'+i+'" class="btn btn-danger btn_remove">X</button></div><div class="form-group" id="dynamic_field"> <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Expiry date</label> <div class="col-md-6 col-sm-6 col-xs-12"><input id="next_expdate" class="form-control col-md-7 col-xs-12" type="text" name="next_expdate[]"> </div> <button type="button" name="remove" id="'+i+'" class="btn btn-danger btn_remove">X</button></div> ');
            //CKEDITOR.replace("next_domainname"  );
        });



        $(document).on('click', '.btn_remove', function(){
            var button_id = $(this).attr("id");
            $('#row'+button_id+'').remove();
        });


        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    });

</script>

{{--<script type="text/javascript"  >
    $(document).ready(function(){
        var i=1;

        $('#ftpadd').click(function(){
            i++;
            $('#dynamic_field2').append('<div id="rowb'+i+'"><br/><div class="form-group" id="dynamic_field2"> <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">FTP username.'+i+'</label> <div class="col-md-6 col-sm-6 col-xs-12"><input id="next_ftpname" class="form-control col-md-7 col-xs-12" type="text" name="next_ftpname[]"> </div> <button type="button" name="remove" id="b'+i+'" class="btn btn-danger btn_remove">X</button> </div></div> ');
            //CKEDITOR.replace("next_domainname"  );
        });



        $(document).on('click', '.btn_remove', function(){
            var button_id = $(this).attr("id");
            $('#rowb'+'b'+button_id+'').remove();
        });


        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    });

</script>--}}

{{--<script type="text/javascript"  >
    $(document).ready(function(){
        var i=1;

        $('#ftppadd').click(function(){
            i++;
            $('#dynamic_field3').append('<div id="row'+i+'"><br/><div class="form-group" id="dynamic_field3"> <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">FTP password.'+i+'</label> <div class="col-md-6 col-sm-6 col-xs-12"><input id="next_ftppassword" class="form-control col-md-7 col-xs-12" type="text" name="next_ftppassword[]"> </div> <button type="button" name="remove" id="'+i+'" class="btn btn-danger btn_remove">X</button> </div></div> ');
            //CKEDITOR.replace("next_domainname"  );
        });



        $(document).on('click', '.btn_remove', function(){
            var button_id = $(this).attr("id");
            $('#row'+button_id+'').remove();
        });


        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    });

</script>--}}

{{--
<script type="text/javascript"  >
    $(document).ready(function(){
        var i=1;

        $('#expadd').click(function(){
            i++;
            $('#dynamic_field4').append('<div id="rowc'+i+'"><br/><div class="form-group" id="dynamic_field4"> <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Expiry date.'+i+'</label> <div class="controls"><div class="col-md-6 xdisplay_inputx form-group has-feedback"><input type="date" class="form-control has-feedback-left" id="next_expdate" name="next_expdate[]" placeholder="First Name" aria-describedby="inputSuccess2Status2"><span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span></div> </div> <button type="button" name="remove" id="c'+i+'" class="btn btn-danger btn_remove">X</button> </div></div> ');
            //CKEDITOR.replace("next_domainname"  );
        });



        $(document).on('click', '.btn_remove', function(){
            var button_id = $(this).attr("id");
            $('#rowc'+'c'+button_id+'').remove();
        });


        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    });

</script>
--}}

{{--<script type="text/javascript"  >
    $(document).ready(function(){
        var i=1;

        $('#skilladd').click(function(){
            i++;
            $('#dynamic_skill').append('<div id="rowc'+i+'"><br/><div class="form-group" id="dynamic_skill"> <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Skill.'+i+'</label> <div class="col-md-6 col-sm-6 col-xs-12"><input id="skill" class="form-control col-md-7 col-xs-12" type="text" name="next_skill[]"> </div> <button type="button" name="remove" id="c'+i+'" class="btn btn-danger btn_remove">X</button></div> ');
            //CKEDITOR.replace("next_domainname"  );
        });



        $(document).on('click', '.btn_remove', function(){
            var button_id = $(this).attr("id");
            $('#rowc'+'c'+button_id+'').remove();
        });


        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    });

</script>--}}

{{--////////////payment script//////////////--}}

<script type="text/javascript"  >
    $(document).ready(function(){
        var i=1;

        $('#payadd').click(function(){
            i++;
            $('#dynamic_field5').append('<div id="rowd'+i+'"><br/><br/><br/><div class="form-group"> <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Payment year</label><div class="controls"><div class="col-md-6 xdisplay_inputx form-group has-feedback"> <input type="date" class="form-control has-feedback-left" id="next_payyear" name="next_payyear[]" placeholder="First Name" aria-describedby="inputSuccess2Status2"> <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span></div> </div> <button type="button" name="remove" id="d'+i+'" class="btn btn-danger btn_remove">X</button></div><div class="form-group"> <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Total amount</label> <div class="col-md-6 col-sm-6 col-xs-12"><input id="next_totalamt" class="form-control col-md-7 col-xs-12" type="text" name="next_totalamt[]"> </div> <button type="button" name="remove" id="d'+i+'" class="btn btn-danger btn_remove">X</button></div><div class="form-group"> <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Received amount</label> <div class="col-md-6 col-sm-6 col-xs-12"><input id="next_receamt" class="form-control col-md-7 col-xs-12" type="text" name="next_receamt[]"> </div> <button type="button" name="remove" id="d'+i+'" class="btn btn-danger btn_remove">X</button></div><div class="form-group" id="dynamic_field5"> <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Remark</label> <div class="col-md-6 col-sm-6 col-xs-12"><input id="next_pendingamt" class="form-control col-md-7 col-xs-12" type="text" name="next_pendingamt[]"> </div> <button type="button" name="remove" id="d'+i+'" class="btn btn-danger btn_remove">X</button></div> ');
            //CKEDITOR.replace("next_domainname"  );
        });



        $(document).on('click', '.btn_remove', function(){
            var button_id = $(this).attr("id");
            $('#rowd'+'d'+button_id+'').remove();
        });


        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    });

</script>



</body>

</html>
