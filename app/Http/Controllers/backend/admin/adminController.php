<?php

namespace App\Http\Controllers\backend\admin;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;

class adminController extends AdminBaseController
{
    public function index(){
        return view($this->_page.'home');
    }

    public function login(){
        return view($this->_page.'login');
    }

    public function login_action(Request $request){
        $credentials = $request->only('email', 'password');

        if (Auth::attempt($credentials)) {
            // Authentication passed...
            return redirect()->intended(route('dashboard'));
        }
        return redirect()->back()->with('success','credentials didnot match!');
    }



    public function logout(){
        Auth::logout();
        return redirect('/@dashboard@');
    }

    public function profile(){

        if (Auth::user()->utype == 'superadmin'){
            $datas=User::where('utype','=','admin')->orWhere('utype','=','editor')->get();
        }
        elseif(Auth::user()->utype == 'admin'){
            $datas=User::where('utype','editor')->get();
        }
        return view($this->_page.'view',compact('datas'));

    }

    public function edit_profile(Request $request){
        return view($this->_page.'edit');
    }

    public function profile_action(Request $request){
        $this->validate($request,['name'=>'required','email'=>'required','profession'=>'required',
            'address'=>'required'],['name.required'=>'name field is required','email.required'=>'email field is required',
            'profession.required'=>'profession field is required','address.required'=>'address field is required']);

        $user=User::findorfail(Auth::user()->id);
        $user->name=$request->name;
        $user->email=$request->email;
        $user->profession=$request->profession;
        $user->address=$request->address;
        if($request->hasFile('image')){
            $file=$request->file('image');
            $filename=time().$file->getClientOriginalName();
            $file->move(public_path().'/backend/images/profile/',$filename);
            $user->image=$filename;
        }
        $user->save();
        return redirect()->back()->with('success','your profile is updated successfully!!');
    }

    public function update_password(Request $request){

        $id=(int)$request->id;
        $this->validate($request,['password'=>'required|min:6|confirmed',
            'oldpassword'=>'required']);
        $data = $request->all();
        $data['password'] = bcrypt($request->password);

        $user = User::find($id);



        if(!Hash::check($data['oldpassword'], $user->password)){

            return redirect()->back()->with('error','oopss!!! old password didnt match!!!');
        }
        else{
            $user->password = $data['password'];
            $user->save();
            return redirect()->back()->with('success','Succesfully Updated!!!');
        }
    }

    public function register(){
        return view($this->_page.'register');
    }

    public function register_action(Request $request){
        $this->validate($request,['name'=>'required','email'=>'required','utype'=>'required','password'=>'required|min:6|confirmed']);
        $user=new User();
        $user->name=$request->name;
        $user->email=$request->email;
        $user->utype=$request->utype;
        $user->password=bcrypt($request->password);
        $user->save();
        return redirect(route('profile'))->with('alert','added successfully!!');
    }

    public function profile_del(Request $request){
        $id=(int)$request->id;
        $user=User::findorfail($id);
        $user->delete();
        return redirect()->back()->with('alert','user deleted successfully!!');
    }
}
